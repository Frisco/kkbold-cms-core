<?php
/*
 * Created on Jun 12, 2009
 */

use \crazedsanity\core\ToolBox;

class TestOfBase extends crazedsanity\database\TestDbAbstract {
	
	
	protected $type = 'mysql';
	protected $user = 'root';
	
	//-------------------------------------------------------------------------
	public function __construct() {
		parent::__construct($this->type, $this->user);
	}//end __construct()
	//-------------------------------------------------------------------------
	
	
	
	
	
	//-------------------------------------------------------------------------
	public function test_connect() {
		
		$this->assertEquals('mysql', $this->type);
		$this->assertTrue(is_object($this->dbObj));
		$this->assertTrue($this->dbObj->is_connected());
//		$this->assertEquals(1, parent::reset_db(__DIR__ .'/../setup/schema.mysql.sql'), ToolBox::debug_print($this->dbObj,0));
	}
	//-------------------------------------------------------------------------
	
	
	
	//-------------------------------------------------------------------------
	public function test_generateSlug() {
		
		$allTests = array(
			'test-slug'					=> "TeST sluG",
			'one-and-another'			=> "one_-and---AnOTHER",
			'b-ad-and-friends'			=> 'B@ad!@#$%^&*()_+-=[]{}\\|/?,.<>friendS.',
			'one-and-two-and-three'		=> 'one&two&three',
		);
//		$this->assertEquals('test-slug', \kkbold\Base::generateSlug("TeST sluG"));
		foreach($allTests as $expected=>$useThis) {
			$this->assertEquals($expected, \kkbold\cms\core\utility::generateSlug($useThis));
		}
	}
	//-------------------------------------------------------------------------
	
	
	
	
}
