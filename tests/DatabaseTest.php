<?php
/*
 * Created on Jun 12, 2009
 */

use \crazedsanity\core\ToolBox;

class TestOfDatabase extends crazedsanity\database\TestDbAbstract {
	
	public function __construct() {
		parent::__construct('mysql', 'root');
	}
	
	public function test_connect() {
		echo __METHOD__ . " - running\n";
		$this->assertEquals('mysql', $this->type);
		$this->assertTrue(is_object($this->dbObj));
		$this->assertTrue($this->dbObj->is_connected());
//		$this->assertEquals(1, parent::reset_db(__DIR__ .'/../setup/schema.mysql.sql'), ToolBox::debug_print($this->dbObj,0));

	}
	
	public function test_core() {
		
		$this->assertEquals('mysql', $this->type);
		$this->assertTrue(is_object($this->dbObj));
		$this->assertTrue($this->dbObj->is_connected());
		
		
		$core = new \kkbold\cms\core\core($this->dbObj, 'test', 'test_id');
	}
}