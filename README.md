# Core Libraries for KKCMS

This is the set of libraries that are required to get an instance of KKCMS off the ground.

## Things of Note

Things have been rewritten and re-organized so they can be included using 
[composer](https://getcomposer.org) via [Packagist](http://packagist.org) so 
dependencies can be handled more easily.

Some things that may have changed:

1. Most anything that might fall into an [anti-pattern](https://en.wikipedia.org/wiki/Anti-pattern)

1. Methods that did database calls like `getMenu` and relied on `global $db`.  
Doing that can create cases of [action at a distance](https://en.wikipedia.org/wiki/Action_at_a_distance_\(computer_programming\))

1. Things that used to be in `Base` are either gone or in `utility`.

## Don't Get Offended

I've written bad code.  Anybody that's written any code as written bad code. 


