<?php

namespace kkbold\cms\core;

use crazedsanity\core\ToolBox;
use crazedsanity\database\Database;

class User {

	public $MM_authorizedUsers = "";
	public $MM_donotCheckaccess = "true";
	public $loginFormAction = "";
	public $MM_fldUserAuthorization = "";
	public $MM_redirectLoginSuccess = '/update/';
	public $table = "admins";
	public $superadmin = 0;
	public $subdirectory = '';
	public $MM_restrictGoTo = '/update/login.php';
	public $db;

	function __construct(Database $db) {
		$this->db = $db;
	}

	// *** Restrict Access To Page: Grant or deny access to this page
	public function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
		// For security, start by assuming the visitor is NOT authorized. 
		$isValid = False;

		// When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
		// Therefore, we know that a user is NOT logged in if that Session variable is blank. 
		if(!empty($UserName)) {
			// Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
			// Parse the strings into arrays. 
			$arrUsers = Explode(",", $strUsers);
			$arrGroups = Explode(",", $strGroups);
			if(in_array($UserName, $arrUsers)) {
				$isValid = true;
			}
			// Or, you may restrict access to only certain users based on their username. 
			if(in_array($UserGroup, $arrGroups)) {
				$isValid = true;
			}
			if(( $strUsers == "" ) && true) {
				$isValid = true;
			}
		}
		return $isValid;
	}

	public function login($loginUsername, $password) {

		// Form Action
		$this->loginFormAction = $_SERVER['PHP_SELF'];
		if(isset($_GET['accesscheck'])) {
			$_SESSION['PrevUrl'] = $_GET['accesscheck'];
		}

		if(isset($loginUsername)) {
//				$loginUsername = $_POST['usrname'];
//				$password = $_POST['passwrd'];
			$MM_redirectLoginFailed = $this->subdirectory . "/update/login.php?alert=" . urlencode("Invalid username or password");

			$sql = "SELECT admin_id, username, password FROM " . $this->table
					. " WHERE username=:user  AND password=:pass";

			$params = array(
				'user' => $loginUsername,
				'pass' => $password,
			);
			try {
				$this->db->run_query($sql, $params);
				$LoginRS = $this->db->get_single_record();
			} catch (Exception $ex) {
				$LoginRS = false;
			}
			if($LoginRS) {
				if(isset($LoginRS['subdomain'])) {
					$loginStrGroup = $LoginRS['subdomain'];
				} else {
					$loginStrGroup = '';
				}

				//declare two session variables and assign them
				$_SESSION['MM_Username'] = $loginUsername;
				$_SESSION['MM_UserGroup'] = $loginStrGroup;
				$_SESSION['MM_UserID'] = $LoginRS['admin_id'];

				if(isset($_SESSION['PrevUrl'])) {
					$this->MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
				}
				ToolBox::conditional_header($this->MM_redirectLoginSuccess);
			} else {
				ToolBox::conditional_header($MM_redirectLoginFailed);
			}
		}
	}

	public function logout() {
		// *** Logout the current user.
		if($this->superadmin) {
			$logoutGoTo = $this->subdirectory . "/update/login.php";
		} else {
			$logoutGoTo = $this->subdirectory . "/";
		}
		if(!isset($_SESSION)) {
			session_start();
		}
		$_SESSION['MM_Username'] = NULL;
		$_SESSION['MM_UserGroup'] = NULL;
		unset($_SESSION['MM_Username']);
		unset($_SESSION['MM_UserGroup']);
		if($logoutGoTo != "") {
			header("Location: $logoutGoTo");
			exit;
		}
	}

	public function restrict() {
		if($this->superadmin) {
			$this->MM_restrictGoTo = $this->subdirectory . '/update/login.php';
		} else {
			$this->MM_restrictGoTo = $this->subdirectory . '/update/login.php';
		}
		if(!( ( isset($_SESSION['MM_Username']) ) && ( $this->isAuthorized("", $this->MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup']) ) )) {
			$MM_qsChar = "?";
			$MM_referrer = $_SERVER['PHP_SELF'];
			if(strpos($this->MM_restrictGoTo, "?"))
				$MM_qsChar = "&";
			if(isset($QUERY_STRING) && strlen($QUERY_STRING) > 0)
				$MM_referrer .= "?" . $QUERY_STRING;
			$this->MM_restrictGoTo = $this->MM_restrictGoTo . $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
			header("Location: " . $this->MM_restrictGoTo);
			exit;
		}
	}

}
