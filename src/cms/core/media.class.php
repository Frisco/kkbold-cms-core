<?php

namespace kkbold\cms\core;

use \PDOException;
use \Exception;


class media extends core {
	protected $db;
	
	const ORPHANFOLDER = "__NO_PARENT__";
	const CHILDFOLDER = "_children";
	
	const INVALIDMIME=1;
	const MISSINGRECORD = 2;
	const MISSINGFOLDER = 3;
	const DELETE_ERR_DB_CONSTRAINT = 4;
	
	public function __construct(Database $db) {
		parent::__construct($db, 'media', 'media_id');
		$this->db = $db;
	}
	
	
	public function createFolder($path, $displayName) {
		$sql = "INSERT INTO media_folders (path, display_name) VALUES 
			(:path, :display)";
		$params = array(
			'path'		=> $path,
			'display'	=> $displayName,
		);
		$newId = $this->db->run_insert($sql, $params);
		
		return $newId;
	}
	
	
	
	public function getAll() {
		
		$sql = "
			SELECT 
				 m.*
				,f.path
				,f.display_name
			FROM 
				media AS m
				INNER JOIN media_folders as f ON (m.media_folder_id=f.media_folder_id)
			ORDER BY
				m.filename, f.path";
		
		
		return $this->db->fetch_array_assoc($sql, 'media_id');
	}
	
	
	public function getAll_structured() {
		$media = $this->getAll();
		$structured = $this->getFolderList();
		$this->debugPrint($structured, "Folder stuff");
		
		foreach($media as $id=>$data) {
			$folderId = $data['media_folder_id'];
			if(isset($structured[$folderId])) {
				$structured[$folderId][self::CHILDFOLDER][$id] = $data;
			}
			else {
				$structured[$folderId][self::ORPHANFOLDER][$id] = $data;
			}
		}
		
		return $structured;
	}
	
	
	
	public function get($id) {
		$sql = "
			SELECT 
				 m.*
				,f.path
				,f.display_name
			FROM 
				media AS m
				INNER JOIN media_folders as f ON (m.media_folder_id=f.media_folder_id)
			WHERE
				m.media_id=:id
			ORDER BY
				m.filename, f.path";
		
		$params = array(
			'id'	=> $id,
		);
		$this->db->run_query($sql, $params);
		
		return $this->db->get_single_record();
	}
	
	
	
	public function getFolderList() {
		$sql = "SELECT * FROM media_folders ORDER BY display_name, path";
		$this->db->run_query($sql);
		return $this->db->farray_fieldnames('media_folder_id');
	}
	
	
	
	public function getFolderOptionList($selected=null) {
		$retval = '';
		$folders = $this->getFolderList();
    
        foreach($folders as $folder){
            $retval .= '<option';
            if($folder['media_folder_id']==$selected || $folder['path'] == $selected){
                echo ' selected="selected" ';
            }
            $retval .= ' value="'.$folder['media_folder_id'].'">'.$folder['display_name'].'</option>';
        }
		
		return $retval;
	}
	
	
	
	public function codeToMessage($code) {
		switch ($code) {
			case UPLOAD_ERR_INI_SIZE:
				$message = "the uploaded file exceeds the upload_max_filesize directive in php.ini";
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$message = "the uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
				break;
			case UPLOAD_ERR_PARTIAL:
				$message = "the uploaded file was only partially uploaded";
				break;
			case UPLOAD_ERR_NO_FILE:
				$message = "no file was uploaded";
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$message = "missing a temporary folder";
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$message = "failed to write file to disk, this probably indicates a permissions issue on the server";
				break;
			case UPLOAD_ERR_EXTENSION:
				$message = "file upload stopped by extension";
				break;

			default:
				$message = "unknown upload error";
				break;
		}
		return $message;
	}
	
	
	
	public function delete($id) {
		$result = 0;
		$data = $this->get($id);
		if(is_array($data)) {
			try {
				$result += $this->deleteRecord($id);
				
			} catch (PDOException $ex) {
				//get the details.
				$msg = $ex->getMessage();
				
				if(preg_match('~cannot delete or update a parent row~i', $msg)) {
					throw new Exception($ex->getMessage(), self::DELETE_ERR_DB_CONSTRAINT, $ex);
				}
				// nothin' doin'.  Throw it again.
				throw $ex;
			}
			$result += $this->deleteFile(ROOT .'/'. $data['path'] .'/'. $data['filename']);
		}
		
		return $result;
	}
	
	
	
	protected function deleteFile($realPath) {
		$result = 0;
		if(file_exists($realPath)) {
			if(unlink($realPath)) {
				$result = 1;
			}
		}
		return $result;
	}
	
	
	protected function deleteRecord($id) {
		$sql = "DELETE FROM media WHERE media_id={$id}";
		try {
			$result = $this->db->query($sql);
		} catch (Exception $ex) {
			$details = "ERROR: ". $ex->getMessage();
			if(preg_match('~integrity constraint violation~i', $ex->getMessage())) {
				$details = "record ID #(". $id .") is referenced in another table";
			}
			throw new Exception($details, self::DELETE_ERR_DB_CONSTRAINT, $ex);
		}
		
		return $result;
	}
	
	
	
	protected function create(array $fieldToValue) {
		$result = $this->db->insert('media', $fieldToValue);
		return $result;
	}
	
	
	
	public function upload($index, $title, $targetPath, $adminId, $user) {
		if(is_array($_FILES) && isset($_FILES[$index]) && $_FILES[$index]['error'] == 0) {
			
			//check if it's an allowed file type.
			$pathinfo = pathinfo($_FILES[$index]['name']);
			$allowed = $this->getAllowedMimes();
			if(isset($allowed[$pathinfo['extension']])) {
				$tmp = $_FILES[$index]['tmp_name'];

				// create a hash of the filename.
				$hash = sha1(file_get_contents($tmp) . microtime(true));

				$newFilename = $hash .'.'. $pathinfo['extension'];
				$insertData = array(
					'filename'			=> $newFilename,
					'admin_id'			=> $adminId,
					'user'				=> $user,
				);

				if(isset($title) && !empty($title)) {
					$insertData['title'] = $title;
				}
				else {
					$insertData['title'] = '(no title)';
				}
				if(isset($targetPath) && !empty($targetPath)) {
					$insertData['path'] = $targetPath;
				}
				else {
					throw new \InvalidArgumentException;
				}

				$insertData['display_filename'] = $this->getUniqName($insertData['path'] .'/'. $_FILES[$index]['name']);

				//
				$fullNewPath = ROOT . $targetPath .'/'. $newFilename;
				$moveRes = move_uploaded_file($tmp, $fullNewPath);
				if($moveRes) {
					$id = $this->create($insertData);
				}
				else {
					throw new Exception(__METHOD__ ." - failed to move file... ");
				}
			}
			else {
				throw new Exception("invalid mime type", self::INVALIDMIME, null);
			}
		}
		else {
			$exceptionDetails = "no files uploaded";
			if(isset($_FILES[$index]) && $_FILES[$index]['error'] != 0) {
				$exceptionDetails = $this->codeToMessage($_FILES[$index]['error']);
			}
			throw new \InvalidArgumentException($exceptionDetails);
		}
		
		return $id;
	}
	
	
	
	public static function clean_name($title) {
		$pathinfo = pathinfo($title);
		$bits = explode('.', $pathinfo['basename']);
		array_pop($bits);
		$newTitle = implode('.', $bits);
		$retval = strtolower(trim(preg_replace('/[^a-zA-Z0-9.-]+/', '-', $newTitle), '-'));
		return $retval .'.'. $pathinfo['extension'];
	}
	
	
	
	public function getByName($path) {
		$info = pathinfo($path);
		
		$dir = self::translate_path($info['dirname'], true);
		$originalFile = $info['basename'];
		$file = self::clean_name($info['basename']);
		
		$sql = "SELECT * FROM protected_media WHERE (display_filename='{$file}' OR display_filename='{$originalFile}') AND path='{$dir}' LIMIT 1";
		
		return $this->db->query_first($sql);
	}
	
	
	
	public function getFolderIdFromPath($path) {
		$fixedPath = $this->cleanPath($path);
		
		$id=$this->_folderIdFromPath($fixedPath);
		
		if($id === false) {
			$bits = explode('/', $fixedPath);
			
			while(count($bits) && $id === false) {
				array_shift($bits);
				$fixedPath = implode('/', $bits);
				
				$id = $this->_folderIdFromPath($fixedPath);
			}
			
			if($id === null) {
				throw new Exception("could not locate folder", self::MISSINGFOLDER);
			}
		}
		
		return $id;
	}
	
	
	
	protected function _folderIdFromPath($path) {
		$sql = "SELECT * FROM media_folders WHERE path=:path ORDER BY media_folder_id, path";
		$params = array(
			'path'	=> $path,
		);
		$this->db->run_query($sql, $params);
		$data = $this->db->farray();
		
		return $data[0];
	}
	
	
	
	public function cleanPath($path) {
		$stripThis = array(
			'~^/',
			'~/$~',
		);
		$cleanedPath = preg_replace($stripThis, '', $path);
		return $cleanedPath;
	}
	
	
	public function update($id, array $data) {
		return $this->db->update('media', $data, 'media_id='. $id);
	}
}
