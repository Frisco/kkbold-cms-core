<?php

namespace kkbold\cms\core;

use stdClass;

class settings {

	function __construct($db, $base) {
		$this->db = $db;
		$this->base = $base;
	}

	public function getAssets() {
		$sql = "SELECT s.asset, a.clean_name
				FROM settings s
				LEFT JOIN assets a ON a.name = s.asset
				GROUP BY s.asset";
		return $this->db->fetch_array($sql);
	}

	function set($value, $name, $asset, $asset_id = 0) {
		$valid = $this->validate($value, $name, $asset, $asset_id);
		if($valid->valid) {
			$sql = <<<SQL
				UPDATE settings
				SET value = '{$valid->value}'
				WHERE name = '{$name}'
					AND asset = '{$asset}'
					AND asset_id = '{$asset_id}'
SQL;
			$qid = $this->db->query($sql);

			if(!$qid) {
				$valid->msg = 'Problem while saving setting.';
			}
		}

		return $valid;
	}

	function get($name, $asset, $asset_id = 0) {
		$sql = <<<SQL
			SELECT s.value, s.type, m.filename
			FROM settings s
			LEFT JOIN media m ON m.media_id = s.value
			WHERE s.name = '{$name}'
				AND s.asset='{$asset}'
SQL;
		if($asset_id) {
			$sql .= " AND s.asset_id='{$asset_id}'";
		}

		$settings = $this->db->fetch_array($sql);



		if(count($settings > 0)) {
			$setting = $settings[0];
		} else if($asset_id) {
			// if no setting for specific asset_id
			// try the default for that asset
			$sql = <<<SQL
				SELECT value
				FROM settings
				WHERE name = '{$name}'
					AND asset='{$asset}'
SQL;
			$setting = $this->db->query_first($sql);
		}



		if($setting['type'] == 'image') {
			//$setting['value']=$setting['filename'];
		}
		$return = new stdClass();
		$return->value = $setting['value'];

		return $return->value;
	}

	function getOptions($setting_option_id) {
		$sql = <<<SQL
			SELECT setting_option_id, `values`, `text`
			FROM setting_options
			WHERE setting_option_id = {$setting_option_id}
SQL;
		$options = $this->db->query_first($sql);

		$return = array();
		if($options['values'] == 'media_folder') {
			$sql = <<<SQL
				SELECT media_id, filename
				FROM media
				WHERE is_folder = 1
				ORDER BY filename ASC
SQL;
			$folders = $this->db->fetch_array($sql);
			if(count($folders) > 0) {
				foreach ($folders as $folder) {
					$return[$folder['media_id']] = $folder['filename'];
				}
			}
		} else if($options['values']) {
			$values = explode(',', $options['values']);

			if($options['text']) {
				$texts = explode(',', $options['text']);
			} else {
				$texts = $values;
			}

			$return = array_combine($values, $texts);

			// make sure everything as a text value
			foreach ($return as $key => $val) {
				if(!$val) {
					$return[$key] = $key;
				}
			}
		}

		return $return;
	}

	function validate($value, $name, $asset, $asset_id = 0) {
		$sql = <<<SQL
			SELECT s.type, s.setting_option_id
			FROM settings s
			WHERE s.name = '{$name}'
				AND s.asset='{$asset}'
SQL;
		if($asset_id) {
			$sql .=" AND asset_id='{$asset_id}'";
		}

		$setting = $this->db->query_first($sql);

		if($setting['type'] != 'wysiwyg') {
			$value = htmlentities($value);
		}

		$valid->value = $value;

		if(!empty($setting['setting_option_id'])) {

			$options = $this->getOptions($setting['setting_option_id']);

			if(in_array($value, array_keys($options))) {
				$valid->valid = 1;
			} else {
				$valid->valid = 0;
				$valid->msg = 'Value not in available options.';
			}
		} else {
			$valid->valid = 1;
		}

		return $valid;
	}

	function input($type, $value, $name, $options = array()) {

		$return = '';
		switch (trim($type)) {
			case 'image': //any single line text value
				$return = $this->base->adminMediaInput($value, '', $name);
				$return.='<input type="hidden" name="settings[' . $name . ']" value="' . $value . '" />';
				break;
			case 'text': //any single line text value
				$return = '<input type="text" class="title" name="settings[' . $name . ']" value="' . $value . '">';
				break;
			case 'textarea': //any multiline text value
				$return = '<textarea class="title" name="settings[' . $name . ']">' . $value . '</textarea>';
				break;
			case 'wysiwyg': //any multiline text value
				$return = <<<EOF
					<textarea class="title" id="editor{$asset}{$asset_id}" name="settings[{$name}]">{$value}</textarea>
					<script type="text/javascript">CKEDITOR.replace( "editor{$asset}{$asset_id}" );
EOF;
				break;
			case 'select': // single select
				if(is_array($options) && count($options) > 0) {
					$return = <<<HTML
						<select class="title" name="settings[{$name}]">
							<option>Select One</option>
HTML;
					$return .= $this->base->makeOptionsList($options, $value);
					$return .= "</select>";
				}
				break;
			case 'radio': //multiple radio buttons
				if(is_array($options) && count($options) > 0) {
					$return = $this->base->makeRadioList('settings[' . $name . ']', $options, $value);
				}
				break;
			case 'checkbox': // multiple checkboxes
				if(is_array($options) && count($options) > 0) {
					$return = $this->base->makeCheckboxList('settings[' . $name . ']', $options, $value);
				}
				break;
		}
		return $return;
	}

	function listing($asset, $asset_id = 0) {
		if($asset_id > 0) {
			$this->assetSettings($asset, $asset_id);
		}

		if(!$asset_id)
			$asset_id = 0;

		$sql = <<<SQL
			SELECT s.name, s.value, s.type, s.title, s.description, s.setting_option_id, m.filename
			FROM settings s
			LEFT JOIN media m ON m.media_id = s.value
			AND s.asset_id={$asset_id}
SQL;

		$settings = $this->db->fetch_array($sql);
		if(count($settings) > 0) {
			$return = "<table class='settings {$asset}'>";
			foreach ($settings as $setting) {
				$return .= <<<HTML
					<tr>
						<td class="name">
							<p><span class="title">{$setting['title']}</span>
HTML;
				if($setting['description']) {
					$return .= '<br><span class="desc">' . $setting['description'] . '</span>';
				}

				if($setting['setting_option_id'])
					$options = $this->getOptions($setting['setting_option_id']);
				else
					$options = '';

				if($setting['type'] == 'image') {
					$setting['value'] = $setting['filename'];
				}
				$input = $this->input($setting['type'], $setting['value'], $setting['name'], $options);

				$return .= <<<HTML
					</p>
					</td>
					<td class="input">
					{$input}
					</td>
					</tr>
HTML;
			}

			$return .= <<<HTML
				</table>
				<input type="hidden" name="asset" value="{$asset}">
				<input type="hidden" name="asset_id" value="{$asset_id}">
		
HTML;
		}

		return $return;
	}

	function save($settings, $asset, $asset_id = 0) {
		$return = '';
		$msg = '';
		$overallerror = false;


		foreach ($settings as $name => $value) {


			if($_FILES[$name]['name']) {
				$media = $this->base->insertMediaAsset($name, 0, 0, 1);

				$value = $media->media_id;
			}

			$status = $this->set($value, $name, $asset, $asset_id);

			if(!$status->valid) {
				$overallerror = true;
				$msg .= $status->msg . '<br>';
			}
		}

		if(!$overallerror) {
			$msg = 'Settings saved.';
		}
		$return->msg = $msg;
		$return->error = $overallerror;

		return $return;
	}

	function assetSettings($asset, $asset_id) {
		//updates settings for the given asset and asset_id,
		//function working correctly depends on the unique key on the settings table
		switch ($asset) {
			case 'galleries':
				$sql = <<<SQL
					INSERT INTO settings (
						SELECT 0, setting_option_id, title, description, `type`, name, value, asset, {$asset_id}, admin, NOW()
						FROM settings
						WHERE asset ='{$asset}'
						AND asset_id = 0
						AND name NOT IN (
							SELECT name
							FROM settings
							WHERE asset='{$asset}'
							AND asset_id='{$asset_id}' )
SQL;
				$this->db->query($sql);
				break;
		}
	}

}
