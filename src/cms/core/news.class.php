<?php

namespace kkbold\cms\core;


class news extends core {
	
	protected $db;
	public $debug=false;
	
	public function __construct(Database $db) {
		parent::__construct($db, 'news', 'news_id');
	}
	
	
	public function get($newsId) {
		$sql = "SELECT 
			 n.* 
			,m.filename
			FROM 
				news AS n
				LEFT OUTER JOIN media AS m ON n.media_id=m.media_id
			WHERE n.news_id={$newsId}";
		
		return $this->db->query_first($sql);
	}
	
	
	public function getAll() {
		$sql = "SELECT 
			 n.* 
			,m.filename
			FROM 
				news AS n
				LEFT OUTER JOIN media AS m ON n.media_id=m.media_id
			ORDER BY title, start_date";
		return $this->db->fetch_array_assoc($sql, 'news_id');
	}
	
	
	public function addTag($id, $tag) {
		\debugPrint(func_get_args(), __METHOD__ ." - arguments");
		$tagObj = new \kkbold\tag($this->db);
		$info = $this->get($id);
		\debugPrint($info, __METHOD__ .' - info');
		return $tagObj->addTag($id, "/news/{$id}", $info['title'], 'news', $tag);
	}
}
