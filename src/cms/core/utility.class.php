<?php

namespace kkbold\cms\core;

use crazedsanity\core\ToolBox;

class utility {

	/**
	 * Immediately prints the supplied data, with formatting to avoid having to 
	 * view page source.
	 * 
	 * @param mixed	$data		Data to be dumped.
	 * @param str	$title		Dump prefixed with this information
	 * @param bool	$printIt	Option to print, overrides "DEBUGPRINT" declaration
	 */
	public static function debugPrint($data, $title = null, $printIt = null) {
		$result = false;
		if(is_null($printIt)) {
			$printIt = ToolBox::$debugPrintOpt;
		}
		if($printIt) {//don't bother doing any work if it's not going to be printed.
			$showTitle = "";
			if(!is_null($title) && strlen($title)) {
				$showTitle = $title . ": ";
			}
			$printData = "";
			if(!is_null($data)) {
				$printData = print_r($data, true);
			}

			ToolBox::debug_print("<b>{$showTitle}</b>{$printData}", $printIt);
			$result = true;
		}
		return $result;
	}

	
	
	/**
	 * Just a time-saver.  Returns a template object for the given asset file.
	 * 
	 * @param str $assetFile	Just use the __FILE__ constant.
	 * @param str $tmplFile		Filename of the template to use/parse
	 * @param str $name			Placeholder name; used when adding to a parent template.
	 * 
	 * @return \crazedsanity\template\Template
	 */
	public static function getAssetTemplate($assetFile, $tmplFile, $name = null) {
		$bits = explode('/assets/', $assetFile);

		$dir = TMPL_DIR . '/assets/' . $bits[1];
		if($tmplFile !== null) {
			$dir .= '/' . $tmplFile;
		}

		$tmpl = new \crazedsanity\template\Template($dir, $name);

		return $tmpl;
	}

	
	
	/**
	 * Okay, this might be more laziness than short-hand... stop judging me.  It works.
	 * 
	 * @param str $tmplFile	Template filename.
	 * @param type $name	Placeholder name; used when adding to a parent template
	 * 
	 * @return \crazedsanity\template\Template
	 */
	public static function getTemplate($tmplFile = null, $name = null) {
		global $_TEMPLATE;
		$path = null;
		if(!is_null($tmplFile)) {
			$path = TMPL_DIR . '/' . $tmplFile;

			// Try finding a ".html" file if the ".tmpl" file does not exist
			if(!file_exists($path) && preg_match('~\.tmpl$~', $path)) {
				$path = preg_replace('~tmpl$~', 'html', $path);
			}
		}
		$tmpl = new \crazedsanity\template\Template($path, $name);
		if(is_array($_TEMPLATE) && count($_TEMPLATE) > 0) {
			$tmpl->addVarList($_TEMPLATE);
		}
		return $tmpl;
	}
	
	
	
	/**
	 * Generates a URL-safe string from a title ("About Stuff" -> "about-stuff")
	 * 
	 * @param string $pageTitle
	 * @return string
	 */
	public static function generateSlug($pageTitle) {
		$pageTitle = str_replace("&", "-and-", $pageTitle);
		preg_match_all("~[\p{Ll}\p{Mn}\d]+~u", strtolower($pageTitle), $out);
		$final = preg_replace('~\-$~', '', preg_replace('~(\-{2,})~', '-', $out[0]));
		return implode('-', $final);
	}

}
