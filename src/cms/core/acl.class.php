<?php

namespace kkbold\cms\core;

class acl extends core {

	// NOTE:: these constants should always match what's in core.php (unless a better system is implemented)
	const ADD		= 4;
	const EDIT		= 6;
	const DELETE	= 7;

	function __construct(Database $db) {
		$this->db = $db;
	}

	public function get($id) {
		throw new BadMethodCallException("not implemented");
	}
	

	function access($admin_id, $asset, $asset_id = 0, $permission = self::EDIT) {

		if(!$admin_id) {
			$admin_id = 0;
		}
		if(is_string($admin_id) && !is_int($admin_id)) {
			$sql = "SELECT admin_id FROM admins WHERE username = '{$admin_id}'";
			$rs = $this->db->fetch_array($sql);
			if(count($rs) > 0) {
				$admin_id = $rs[0]['admin_id'];
			} else {
//				$this->debugPrint($rs, "no access, not a valid user");
				return false;
			}
		}

		if(!$asset_id) {
			$asset_id = 0;
		}
		$sql = "SELECT admin_id, asset, asset_id, permission
			FROM acl
			WHERE group_id IN(SELECT group_id
							  FROM admin_groups
							  WHERE admin_id = {$admin_id})
				AND (asset_id = {$asset_id} OR asset_id = 0)
				AND asset = '{$asset}'
				AND permission = {$permission}";
		$grouplevel = $this->db->fetch_array($sql);

		if(count($grouplevel) > 0) {
			return true;
		} else {
			$sql = "SELECT admin_id, group_id, asset, asset_id, permission
				FROM acl
				WHERE admin_id = {$admin_id}
					AND group_id = 0
					AND asset_id = {$asset_id}
					AND asset = '{$asset}'
					AND permission = {$permission}";
			$adminlevel = $this->db->fetch_array($sql);
			$this->debugPrint($sql, "SQL");

			if(count($adminlevel) > 0) {
				return true;
			}
		}

		$this->debugPrint(func_get_args(), "no access, couldn't find anything");
		return false;
	}

	public function hasAdd($asset, $asset_id = 0) {
		return $this->access($_SESSION['MM_Username'], $asset, $asset_id, self::ADD);
	}

	public function hasEdit($asset, $asset_id = 0) {
		return $this->access($_SESSION['MM_Username'], $asset, $asset_id, self::EDIT);
	}

	public function hasDelete($asset, $asset_id = 0) {
		return $this->access($_SESSION['MM_Username'], $asset, $asset_id, self::DELETE);
	}

	public function hasAccess($asset, $asset_id = 0) {
		$result = $this->canModify($asset, $asset_id);
		return $result;
	}

	public function canModify($asset, $asset_id = 0) {

		$result = (
				$this->access($_SESSION['MM_Username'], $asset, $asset_id, self::ADD) || $this->access($_SESSION['MM_Username'], $asset, $asset_id, self::EDIT) || $this->access($_SESSION['MM_Username'], $asset, $asset_id, self::DELETE)
				);


		return $result;
	}

	public static function accessDeniedMsg($useHtml = true) {
		$msg = 'You do not have access to this area.  If this is a mistake please contact your site administrator.';
		if($useHtml) {
			$msg = "<p style='color: #f00; font-weight: bold;'>{$msg}</p>";
		}
		return $msg;
	}

}
