<?php

namespace kkbold\cms\core;

class page extends core {
	protected $db;
	protected $data = array();
	
	public function __construct(Database $db) {
		parent::__construct($db, 'pages', 'page_id');
	}
	
	
	/*
	 * Searches the pages table as well as tags for the given term.
	 */
	public function searchPages($searchTerm, array $filters = null) {
		$sql = "
			SELECT 
			DISTINCT(p.page_id), p.title, p.url, p.created, p.modified, date(p.created) as display_date,
			(
				SELECT group_concat(t.tag SEPARATOR '|') FROM tags t WHERE
					t.id=p.page_id AND t.category='pages'
			) AS alltags
			FROM 
				pages p 
				LEFT OUTER JOIN tags t ON (t.id=p.page_id AND t.category='pages')
			WHERE 
				status='active'
				AND p.title LIKE '%". $searchTerm ."%'
				OR p.body LIKE '%". $searchTerm ."%'
				OR t.tag LIKE '%". $searchTerm ."%'
			ORDER BY title";
		
		$result = $this->db->fetch_array($sql);
		
		//break the tags into an array.
		foreach($result as $i=>$data) {
			$result[$i]['alltags'] = explode('|', $data['alltags']);
		}
		
		if(is_array($filters) && count($filters)) {
			$filterObject = new \kkbold\filter($this->db);
			$result = $filterObject->filter($result, $filters);
		}
		
		return $result;
	}
	
	
	public function getAll() {
		$sql = "
			SELECT 
				p.*,
				(
					select count(page_id) FROM pages p2 WHERE p2.parent_id=p.page_id
				) as _num_children
			FROM 
				pages as p
			ORDER BY 
				parent_id, 
				sort ASC, 
				title ASC";
		
		$data = $this->db->fetch_array_assoc($sql, 'page_id');
		
		foreach($data as $i=>$x) {
			$data[$i]['_depth'] = self::getPageDepth($i, $data);
		}
		
		$this->data = $data;
		
		return $data;
	}
	
	
	public function get($id, $onlyActive=true) {
		$sqlOnlyActive = "";
		if($onlyActive) {
			$sqlOnlyActive = " AND p.status = 'active'";
		}
		$sql = "SELECT 
				p.*,
				(
					select count(page_id) FROM pages p2 WHERE p2.parent_id=p.page_id
				) as _num_children,
				m.filename AS og_image_filename,
				m.filename AS filename
			FROM 
				pages as p
				LEFT OUTER JOIN media AS m ON (p.og_image_media_id=m.media_id)
			WHERE
				p.page_id={$id}{$sqlOnlyActive}";
		return $this->db->query_first($sql);
	}
	
	
	public static function getPageDepth($id, &$allData) {
		$depth = 0;
		$trace = "";
		while ($id != 0 ) {
			$oldId = $id;
			$id = self::getParent($id, $allData);
			if($depth == 0) {
				$trace = $oldId;
			}
			$trace .= ":". $id;
			$depth++;
			if($depth > count($allData)) {
				debugPrint("She's borken, captain");
				exit;
			}
		}
		
		
		return ($depth -1);
	}
	
	
	public function delete($id) {
		$record = $this->get($id);
		$result = null;
		
		//ONLY delete if there are no children, we don't need orphans.
		if($record['_num_children'] == 0) {
			$sql = "DELETE FROM pages WHERE page_id={$id}";
			$this->db->query($sql);
			
			$result = $this->db->affected_rows;
		}
		
		return $result;
	}
	
	
	public function addTag($id, $tag) {
		\debugPrint(func_get_args(), __METHOD__ ." - arguments");
		$tagObj = new \kkbold\tag($this->db);
		$info = $this->get($id);
		return $tagObj->addTag($id, $info['url'], $info['title'], 'pages', $tag);
	}
	
	
	public function getByUrl($url, $onlyActive=true) {
		$sqlOnlyActive = "";
		if($onlyActive) {
			$sqlOnlyActive = " AND p.status = 'active'";
		}
		
		if(preg_match('~/$~', $url) == 1) {
			$firstUrl = $url;
			$secondUrl = preg_replace('~/$~', '', $url);
		}
		else {
			$firstUrl = $url .'/';
			$secondUrl = $url;
		}
		$urlSql = "(p.url='{$firstUrl}' OR p.url='{$secondUrl}')";
		$sql = "SELECT 
				p.*,
				(
					select count(page_id) FROM pages p2 WHERE p2.parent_id=p.page_id
				) as _num_children,
				m.filename AS og_image_filename,
				m.filename AS filename
			FROM 
				pages as p
				LEFT OUTER JOIN media AS m ON (p.og_image_media_id=m.media_id)
			WHERE
				{$urlSql}{$sqlOnlyActive}";
				$this->debugPrint($sql, "SQL");
		return $this->db->query_first($sql);
	}
	
	
	public function getByAsset($assetName) {
		$sql = "SELECT 
				p.*,
				(
					select count(page_id) FROM pages p2 WHERE p2.parent_id=p.page_id
				) as _num_children
			FROM 
				pages as p
			WHERE
				p.asset=:asset AND p.status = 'active'";
		$params = array(
			'asset'	=> $assetName,
		);
		$this->db->run_query($sql, $params);
		return $this->db->get_single_record();
	}
	
	
	
	public function getHomepage() {
		return $this->getByAsset('home');
	}
	
	
	/**
	 * Determines what the proper URL for the given ID should be... 
	 * 
	 * @param type $pageId
	 * 
	 * @return string
	 * @throws \LogicException
	 */
	private function getRealUrl($pageId) {
		if(!isset($this->_info[__FUNCTION__])) {
			$this->_info[__FUNCTION__] = 0;
		}
		$allParents = $this->getParents($pageId, $this->data);
		
		$realUrl = "";
		foreach(array_reverse($allParents) as $id) {
			$cleanTitle = $this->getCleanTitle($id);
			$realUrl .= '/'. $cleanTitle;
		}
		$realUrl .= '/'. $this->getCleanTitle($pageId);
		$realUrl = preg_replace('~\/{2,}~', '/',  $realUrl);
$this->debugPrint($realUrl, "dynamic URL");
		
		return $realUrl;
	}
	
	
	public function create(array $data) {
		if(isset($data['clean_title'])) {
			unset($data['clean_title']);
		}
		$insertData = $data;
		
		$parentId = 0;
		$this->getAll();
		if(isset($data['parent_id']) && is_numeric($data['parent_id'])) {
			$parentId = $data['parent_id'];
		}
		$insertData['url'] = preg_replace('~//~', '/', $this->getRealUrl($parentId) .'/'. self::makeCleanTitle($insertData['title']) .'/');
		
		//TODO: check for existing pages with the same URL...
		
		$newId = $this->db->insert('pages', $insertData);
		return $newId;
	}
	
	
	public function update(array $data, $pageId) {
		if(!is_numeric($pageId) || (is_numeric($pageId) && $pageId < 1)) {
			throw new \InvalidArgumentException("invalid pageId");
		}
		if(!is_array($data) || !count($data)) {
			throw new \InvalidArgumentException("no changes to perform");
		}
		
		if(isset($data['clean_title'])) {
			unset($data['clean_title']);
		}
		$updateRes = 0;
		$oldRecord = $this->get($pageId);
		
		$parentId = $oldRecord['parent_id'];
		if(isset($data['parent_id'])) {
			$parentId = $data['parent_id'];
		}
		$this->getAll();
		if(!isset($data['url']) || empty($data['url'])) {
			$baseUrl = "";
			if(isset($parentId) && $parentId > 0) {
				$baseUrl = $this->getRealUrl($parentId);
			}
			$data['url'] = $baseUrl .'/'. self::makeCleanTitle($data['title']) .'/';
		}
		$data['url'] = preg_replace('~/{2,}~', '/', $data['url']);
		
		$updateRes += $this->db->update('pages', $data, "page_id=" . $pageId);
		$this->getAll();
		
		$oldUrl = $oldRecord['url'];
		
		//update all the affected children.
		$findKidSql = "SELECT page_id, parent_id, title, url FROM pages WHERE (url LIKE '{$oldUrl}%' OR parent_id={$pageId}) AND page_id <> {$pageId}";
		$children = $this->db->fetch_array_assoc($findKidSql, 'page_id');
		
		foreach($children as $id=>$child) {
			$newUrl = $this->getRealUrl($id);
			$updateRes += $this->db->update('pages', array('url'=>$newUrl), 'page_id='. $id);
		}
		
		if(debugPrint($updateRes, __METHOD__ ." - update result")) {
			\debugPrint($children, "these children should be updated");
			\debugPrint($this->db->history, __METHOD__ ." - history");
			exit;
		}
		
		return $updateRes;
	}
	
	
	/**
	 * Retrieve's the given page's cleaned title, in case it was somehow different 
	 * than what we expect (if it was previously "cleaned" in a different way).
	 * If given ID=0, it returns an empty string.
	 * 
	 * @param type $id	Page ID to look up in cache.
	 * 
	 * @return string	The given page's cleaned title.
	 */
	public function getCleanTitle($id) {
		$retval = "";
		if($id > 0) {
			$data = $this->data[$id];
			$bits = explode('/', preg_replace('~/$~', '', $data['url']));
			$retval = $bits[count($bits) -1];
		}
		return $retval;
	}
	
	

	public static function makeCleanTitle($url) {
		$url = strtolower( $url );
		$url = preg_replace('~^/~', '', $url);
		$url = preg_replace('~/$~', '', $url);
		$url = str_replace( ' ', '-', trim( $url ) );
		$url = str_replace( '/', '-', trim( $url ) );
		$url = preg_replace( "/[^\/A-Za-z0-9_-]/", "", $url );
		
		return $url;
	}
	
	
	public function getChildPages($parentId) {
		$sql = "SELECT 
				p.*,
				(
					select count(page_id) FROM pages p2 WHERE p2.parent_id=p.page_id
				) as _num_children
			FROM 
				pages as p
			WHERE p.parent_id=:pid";
		$params = array(
			'pid'	=> intval($parentId),
		);
		
		$this->db->run_query($sql, $params);
		$data = $this->db->farray_fieldnames($this->pkey);
//		$data = $this->db->fetch_array_assoc();
//		return $this->farray_fieldnames($indexField);
		
		return $data;
	}
	
	
	public static function getBodyPreview($body, $minWords=50) {
		$matches = array();
		preg_match_all('/<p[^\>]*>(.*)<\/p\s*>/i', $body, $matches);
		
		$useThis = array();
		if(intval($minWords) <= 0) {
			$minWords = 50;
		}
		$curWords = 0;
		
		
		$preview = $body;
		if(count($matches[0]) > 1) {
			foreach($matches[0] as $k=>$v) {
				$curWords += str_word_count($v);
				$useThis[$k] = $v;
				if($curWords >= $minWords) {
					break;
				}
			}
			$preview = implode($useThis);
		}
		
		return $preview;
	}
}
